# Examples

## Return All Frames (RAF) Example

To create a service for receiving all return frames, we create a RAF user. Then we bind to the remote SLE provider and start the reception of frames.

```python
import sle

sle_service = sle.RafServiceUser(
    service_instance_identifier=SI_IDENTIFIER,
    responder_host=RESPONDER_HOST,
    responder_port=RESPONDER_PORT,
    auth_level="NONE",  # or "ALL" or "BIND"
    local_identifier=LOCAL_IDENTIFIER,
    peer_identifier=PEER_IDENTIFIER,
    local_password=LOCAL_PASSWORD,
    peer_password=PEER_PASSWORD
)

sle_service.frame_indication = lambda frame: print(frame)

sle_service.bind()
sle_service.wait_for_state(sle.SleState.READY)

sle_service.start()
sle_service.wait_for_state(sle.SleState.ACTIVE)

input("Press <Enter> to stop")

sle_service.stop()
sle_service.wait_for_state(sle.SleState.READY)

sle_service.unbind()
sle_service.wait_for_state(sle.SleState.UNBOUND)
```

## SLE Test Tool

This Python package comes with a command line SLE test tool. When installed via pip,
you can run the test tool as follows:

```bash
slet <config-file> [dump-file]
```

The tool expects one mandatory parameter pointing to a config file, and an optional
arguments, which is the file to which received frames are written (if ommitted, they
will be printed on screen).

The config file shall be using Yaml format and shall provide the configuration
for each available SLE link.

In the provided `config.yml` there are two examples configured:

- VST: The VisionSpace SLE provider: https://github.com/visionspacetec/sle-provider
- SLEAPI-J: The ESA SLE API package: https://github.com/esa/sleapi-j (A dockerized version can be found here: https://gitlab.com/libredocker/docker-sleapi-j)
