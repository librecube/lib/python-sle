# Python SLE

Implementation of the CCSDS Space Link Extension (SLE) API in Python. It
provides the [RAF (Return All Frames)](docs/911x1b5.pdf) and
[RCF (Return Channel Frames)](docs/911x2b2.pdf) services and the
[CLTU Forward Link](docs/912x1b5.pdf) service.

The CCSDS Space Link Extension (SLE) services are used by all major space
agencies to interconnect ground stations to mission control systems. The
SLE is a standardized protocol that enable such cross-support. In basic terms,
a groundstation communicates with a spacecraft through CCSDS telecommand and
telemetry frames. The transfer of those frames between a groundstation and
a remote mission control system is done via SLE, which is essentially just a
container protocol that runs over [TCP/IP](docs/913x1b2.pdf).
On the side of the groundstation there sits a SLE provider gateway and on
the mission control side there is a SLE user gateway.

This Python package implements the SLE User API and can be used to develop
SLE user and provider applications.

## Installation

Install via pip:

```bash
$ pip install sle
```

## Examples

See the [examples](examples/README.md).

## Documentation

The API documentation is in [docs/README.md](docs/README.md).

The user manual for [`pyasn1` is here](https://www.digital-experts.de/doc/python-pyasn1/pyasn1-tutorial.html).

## Contribute

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube Documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
